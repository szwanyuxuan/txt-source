１１

隔天，兩人下到了百二十一階層。

百二十一階層的構造和百二十階層相同。換句話說，只有五間房間。其中一個是有階梯的房間。進入了剩下的四間房間中，右邊靠前的房間的侵入通路。

「嗯？」
「怎麼了嗎」
「魔獸是出現了，但只有一隻」
「嘿？」

從這階層開始，不論進入幾人都只會出現一隻嗎。若是如此，難易度就會下降很多。但真有這種事嗎。這一隻，說不定難纏得不得了。
今天吊在腰上的是〈威力劍〉。是在百階層得到的。能辦到萬能的用法，在身上的劍之中有著最高的攻擊威力。但是，〈澤娜的守護石〉的物理攻擊力附加是無效的。

「呼嗯。身體的大小，跟百二十階層沒什麼差啊。身高跟我差不多。魔力很強。用的是短劍吧」
「知道了」
「〈展開〉！」

雷肯展開了〈沃爾肯之盾〉。就算想東想西也沒用。反正不戰鬥看看就不知道對手的強度跟特性。

進入房間後站住。
阿利歐斯馬上跟著進入。
敵人在房間中央自然地站著，右手握著短劍。
身高很長，所以短劍看起來小得不相稱。

（嗯？）
（那把短劍）
（有在哪裡見過嗎？）

短劍放出了白光，劍身的長度伸長了三倍。
雷肯感覺到全身發冷。

「是〈彗星斬〉！」
「誒誒？」
「把那劍身當作，還能再伸長那長度的一倍！在一瞬間」
「好的」

通知危機的鐘聲，在雷肯的腦內鳴響。
對手跑向這邊。

被逼到牆邊可受不了。雷肯也跑了起來。阿利歐斯從旁邊衝了出去。阿利歐斯很快。衝到了雷肯前方。
到達了大概還有十步的距離時，魔獸將劍拉向右方。然後從右向左橫掃。在雷肯兩人眼裡就是從左到右的攻擊。

魔法劍的劍身突然伸長，在要一刀兩斷左側的阿利歐斯的前一刻，阿利歐斯跳躍了。雷肯將盾的高度對準，擋下了魔法劍的攻擊。

應該擋下了才對。
但是魔法劍的劍尖，砍裂了雷肯的右胸和右肩。

跳到空中的阿利歐斯，對魔獸的頭部放出斬擊。但是魔獸展現了驚人的反應，退到斜後方避開了。但還是削掉了魔獸的臉的右上部。

下一瞬間，雷肯逼近魔獸，對頭部放出了突刺之斬擊。
差了一步，沒砍下頭。

魔獸揮了魔法劍。但雷肯接近到了極近的距離，盾牌已經逼近了敵人。金屬聲鳴響，魔法劍被〈沃爾肯之盾〉擋下了。
在魔獸的斜後方著地的阿利歐斯，以反作用力在向右迴轉的同時跳回後方，砍飛了魔獸的頭。

勝負結束。
框啷，劍從雷肯的手落下。無力地垂下的右手滴出了血。
放下左手放開手掌，〈沃爾肯之盾〉敲出聲音倒在地上。雷肯彎下雙膝跪地，左手伸向右胸，詠唱咒文。

「〈回復〉」

接著也對右肩施展回復。

「〈回復〉」

又一次，對右胸和右肩施展回復。

「〈回復〉〈回復〉」
「還好嗎？」

阿利歐斯一臉擔心地靠近。

「你，不是被砍到⋯」

魔獸揮出的魔法劍的劍尖，看起來劃過了阿利歐斯的腳，看向正在靠近的阿利歐斯，雷肯無語了。右腳的靴子被深深砍開。預測到雷肯的擔憂，阿利歐斯說道。

「沒問題的。有在嘴裡含著大紅藥水突進。在跳躍的瞬間喝下了。靴子雖然裂開了，但裡面的腳沒事」
「〈回復〉」

雖然這麼說，但還是給了回復。

「有用盾牌好好擋下才對啊」
「用盾牌擋下的部分沒事不是嗎？然後魔法劍的軌道通過盾牌的時候，劍尖就再次出現了。那劍尖」
「就達到了我的胸口嗎。原來如此」

了解之後，這也是理所當然的。魔法劍的劍身，除了根部的短劍部分以外都是由魔法構成的。魔法碰到盾牌的話，盾牌這邊雖然會有衝擊，但劍那邊沒有。又或者幾乎沒有。然後，用盾牌擋下的劍尖雖然失去了蹤跡，但劍到達了沒有盾牌的位置的話，劍尖就會現形。就只是這樣而已。

第二次用盾牌來擋的時候，還好距離很近。由於靠近到了極近距離，所以能以盾牌碰到並推開物理性質的劍身，而非由魔法劍的魔法構成的劍身。這決出了勝負。

身在承受側的話，所謂魔法劍，真的很麻煩。畢竟，〈立體知覺〉探知不了。〈立體知覺〉是探知物理性存在之物的技能，探知不了魔法。然而，〈魔力感知〉沒有〈立體知覺〉那麼精密，也應付不了高速戰鬥。說到底，〈魔力感知〉並非那種能力。

但是，贏了。
總之是贏了。

雷肯靠近寶箱，打開蓋子。