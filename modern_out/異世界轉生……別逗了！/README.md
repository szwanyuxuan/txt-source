# novel

- title: 異世界転生…されてねぇ！
- title_zh: 異世界轉生……別逗了！
- author: タンサン
- illust:
- source: http://ncode.syosetu.com/n0865em/
- cover: https://images-na.ssl-images-amazon.com/images/I/91g7eYbDP3L.jpg
- publisher: syosetu
- date: 2019-12-29T05:14:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts

- 航島カズト
- 夕薙
- 

## publishers

- syosetu

## series

- name: 異世界転生…されてねぇ！

## preface


```
为即将开始的高中校园生活而欢欣雀跃的主人公结城幸助（ゆうき•こうすけ）却因救人而丢掉了性命。

然而，这样的他人生并未就此终结。

所救助的老爷爷竟然刚好是神灵大人，作为谢礼演变成了让他转生去异世界这样的事。

如同最喜欢的异世界小说那般，带着开挂的状态转生，主人公为此兴奋不已。可是，欢喜的他醒来的地方，却是太平间。

这就是一个保留着开挂能力，在现代复活的主人公打算就这样平静的活下去的故事。

……话虽如此，因为被卷入各种事件，多少也有些战斗情景。

---

書籍発売中！コミカライズも開始いたしました！


　これから始まる高校生活に胸躍らせる主人公、結城幸助（ゆうき・こうすけ）は、人助けの末に命を落とした。

　だが、そんな彼の人生はまだ終わらない。

　助けたお爺さんは偶然にも神様であり、そのお礼として異世界へ転生させてもらう事となったのだ。
　大好きな異世界小説のように、チート能力を備えた状態での転生に喜ぶ主人公。しかし、そんな彼が目覚めた場所は、遺体安置所だった。

　チート能力をそのままに、現代に蘇生した主人公が平穏に生きていこうとする物語。

　……と言いつつも、色々な事件に巻き込まれるため、戦闘シーンも多々あります。
　第一章「陰陽術編」、第二章「異能編」、第三章「魔術編」は終了。第四章「一般編」投稿中です。


　また、主婦と生活社のPASH！ブックス様から書籍版発売中です！
　イラストは夕薙様が描いてくださりました。
　第１部分の「キャラクターデザイン（イラスト付き）」にイラストが載っていますので、見ていただけると嬉しいです。
　さらに、航島カズト先生によるコミカライズも開始しました！そちらも読んでいただけるとありがたいです！

　下記のURLから公式ページへ飛べます。
　https://pashbooks.jp/series/saretenee/532/

　コミカライズの閲覧はこちらのURLです。
　http://comicpash.jp/saretenee/

　ＷＥＢ版、書籍版、コミカライズともに応援よろしくお願いいたします！
```

## tags

- node-novel
- R15
- syosetu
- オリジナル戦記
- チート能力
- ファンタジー
- ローファンタジー
- ローファンタジー〔ファンタジー〕
- 主人公最強
- 伝奇
- 書籍化
- 残酷な描写あり
- 異能
- 異能力バトル
- 転生失敗
- 陰陽術

# contribute

- 水無月流歌
- 东山奈央
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n0865em

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n0865em&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n0865em/)
- https://masiro.moe/forum.php?mod=forumdisplay&fid=136
- https://comic.pixiv.net/works/6184
- https://comicpash.jp/saretenee/
- [英雄私生子的英雄谭吧](https://tieba.baidu.com/f?kw=%E8%8B%B1%E9%9B%84%E7%A7%81%E7%94%9F%E5%AD%90%E7%9A%84%E8%8B%B1%E9%9B%84%E8%B0%AD&ie=utf-8)
- https://lhscan.net/manga-isekai-tensei-saretane-raw.html
- 


