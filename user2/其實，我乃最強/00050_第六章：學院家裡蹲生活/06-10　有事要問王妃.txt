我來看望我的親生母親，坦白地說，我不想和這個阿姨有太多的關係。因為她對我和我現在的家人（芬菲斯家族）做了很多壞事。

只是，要這位阿姨從舞台上退場有點困難。
因為國內可能會陷入動亂。因此，直到夏洛特成年為止，為了維持國內勢力的均衡，必須讓她活著。

然而，即使她戴上了項圈，野心也沒有減弱，還在暗地裡和魔人聯繫。真頑強。

然後，她又和學院裡的搞笑集團接觸了。

不管和他們有什麼企圖，對我來說都無所謂，但問題是夏洛特把那個搞笑的學生集團定為了下一個遊戲玩伴。
為了確保妹妹的安全，我必須好好地工作。

「妳知道阿列克謝・古貝爾克嗎？」

頓時，警戒MAX的吉澤洛特的眉毛跳了一下。

「……太突然了。對，我知道。他不僅是古貝爾克伯爵家的繼承人，以他的魔法力量來說，現在還是學院裡最有名的英才呢。」

果然，他是個厲害的人啊。

「我聽說妳很快就要與他會面了。」

「他是古貝爾克領主在王都的嫡子。有權勢的貴族和王族交換意見並不稀奇。」

「不是代替貴族家領主會面，而是作為『號碼』的代表，對吧？」

吉澤洛特狠狠地瞪著我。好可怕。

「……對，一切都在你預料之中吧。」

一口喝乾玻璃杯裡的東西，吉澤洛特深深地坐在沙發上。

「你和芬菲斯卿關係很好。也就是說，你是國王派。」

妳突然在說什麼？

「呋呋呋。就算是你，似乎也會害怕身為王妃的我與貴族派聯手吧。」

所以妳在說什麼？

「好吧，那我們做個交易。我拒絕他們的提議，你就把這個項圈――！？」

碰的一聲，吉澤洛特的頭飛到正上方，「哎呀！」頭撞到了天花板。掉下來的頭和身體互相吸引，一下子就回到原來的位置。

「什、什……」

「妳不知道自己的立場嗎？能和我對等的交易嗎？」

「那你來這裡有什麼事！？」

不，為了騷擾『１』的人，阿列克謝前輩，我來對這傢伙說『不要聯手』。雖然在那個意義上是對的。

「哦，我明白了。」

吉澤洛特用手按住撞到的地方，臉上露出了微笑。

「你是想讓我和他們聯手，再從他們那裡得到情報嗎？」

我沒有那個想法啊。這個傢伙在邪惡的地方比我厲害。

「好啊，既然是這個時候，就用它吧。我一開始也是這麼打算的。」

如果跟不上話保持沉默，就會順利的進行著。
但是，可以吧？雖然與預定不同，不過說不定可以知道號碼們的意圖。

如果變成這一點，要先打預防針嗎？

「別讓學生遭遇危險。」

看來這傢伙好像還不知道夏爾加入號碼。其他的成員雖然無所謂，不過他們姑且是夏爾現在的遊戲對象，不想變成錯綜複雜的狀況。

「什麼意思？他們對你來說，應該是敵視的貴族派的孩子們吧？」

我不知道什麼貴族派。那麼，該如何解釋呢？
如果考慮的話，好像又隨意地達到了突然的答案。

「……原來如此。說到底，你的敵人是在他們背後暗中活動的『教團』吧。」

又出現了一個奇怪的名詞。教團是什麼？

「如果是這樣的話，我也可以和你一起並肩戰鬥嗎？也許你誤會了，我提供資金是為了利用那些人，而不是為了認同那個教義。」

「我沒打算和你相處。」

總之先把真心說得帥氣。哼哼，王妃擦了擦。 
嘛，隨心所欲的話以後可能也很麻煩，還是趁機再先打預防針吧。

「順便給你個忠告吧。教團可不是像你這樣的人對付得了的對手。豈止是會被絆倒，連綿延不絕的生命也會被自己捨棄。」

吉澤洛特的臉很強硬。
說實話，雖然不知道什麼是教團什麼的，只是隨便裝作而已，但是效果好像相當不錯。

「你知道到哪裡了？路西法拉教團的事，到哪裡去了……」

路西法拉？好像在哪裡聽到的……啊——。

「魔神……」

的名字吧。裝扮成巴爾．阿戈斯的貴族的魔人說了。讓它復活什麼的。

「啊！？」

嗯？總覺得吉澤洛特的臉變蒼白了？我剛才說了什麼了嗎？算了吧。

「我已經安排好了。跟號碼沒有關係，那邊的窗口只有古貝爾克一個人，可以查到情報。」

「……爽快地追加了條件。」

如果事態變得很糟糕，和其他成員接觸，然後和夏爾見面，會很困擾。這個女人對情操教育不好。

「那麼，再見了。」

用光學迷彩結界突然消失，這傢伙驚訝地睜開眼睛。我從悄悄進來的窗戶裏不發出聲音走了出去。然後靜靜地關上了窗戶——。



★★★



化身濕婆的黑衣男消失了。
吉澤洛特警戒了一段時間，終於放鬆了身體。直截了當，頭裡的疼痛告訴了自己。

我絕對無法贏過他。

只要是對峙過一次的人，這種恐懼就會像詛咒一樣侵蝕全身。
無窮無盡的魔力，不可思議的魔法。
即使是被譽為當代最強的她，過去也有過靠個人力量無法戰勝的對手。魔王就是這樣。
可是那個還是作為群體能處理的範疇。實際上，贏了。

（但是，那個男人……）

即使動員了國內所有精銳部隊，不但沒有獲勝，反而有可能成為真正的『戰爭』。
如果有能夠對等戰鬥的對手的話，那也只有神代的怪物了吧。

如果有的話――。

「魔神……」

濕婆不經意說出的話。
按住疼痛的頭，吉澤洛特扭曲地笑了。

到現在為止，不管再怎麼探索，也不知道路西法拉教團的真正目的。但是在有關教團的話題中，卻出現了『魔神』這個名詞，她立刻想到了他們的目的。

「你們打算讓魔神復活吧……」

如果能夠得到那份力量的話。
吉澤洛特悄悄地把手放在了項圈上。

「也許能戰勝那個男人……」

痛。頭裡產生了劇痛。頭疼得像要裂開似的。

「什、什麼……？」

並且，伴隨著疼痛，在腦海中迴響的聲音。

『啊，那種渴望，如同燃燒殆盡的怨恨。總算找到了』

「誰……？到底說什麼……？」

『聽見我聲音的人啊，你的願望，由我來實現吧。快點成為我附身的對象吧！』

「不，等一下――」



事到如今，拒絕是沒有意義。既然許了一次願，就再也無法對抗它了。

吉澤洛特雙眼被扭轉，意識被收割了――。